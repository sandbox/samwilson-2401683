<?php

/**
 * @file
 * The install file for the PRINCE2 module.
 */

/**
 * Install all fields and field instances.
 *
 * Note that node types are defined by prince2_node_info() in prince2.module.
 */
function prince2_install() {

  // Create vocabularies.
  foreach (_prince2_install_vocabs() as $vname => $vocab) {
    $vocab_id_var = _prince2_vocab_varname($vname);
    $existing = taxonomy_vocabulary_load(variable_get($vocab_id_var, 0));
    if ($existing) {
      continue;
    }
    $vocab['module'] = 'prince2';
    $vocab['machine_name'] = $vname;
    $vocabulary = (object) $vocab;
    unset($vocabulary->fields);
    taxonomy_vocabulary_save($vocabulary);
    variable_set($vocab_id_var, $vocabulary->vid);
    if (isset($vocab['fields']) && is_array($vocab['fields'])) {
      foreach ($vocab['fields'] as $field) {
        $field['bundle'] = $vname;
        $field['entity_type'] = 'taxonomy_term';
        field_create_field($field);
        field_create_instance($field);
      }
    }
  }

  // Create content types.
  foreach (_prince2_install_node_types() as $node_type => $fields) {
    foreach ($fields as $field) {
      $field['locked'] = TRUE;
      field_create_field($field);
      $field['entity_type'] = 'node';
      $field['bundle'] = $node_type;
      field_create_instance($field);
    }
  }
}

/**
 * Get a vocabulary's VID-variable name.
 *
 * Each vocabulary has its VID stored as a Drupal variable. This function gives
 * this variable's name (to to avoid typos).
 *
 * @param string $name
 *   The machine name of the vocabulary
 *
 * @return string
 *   The name of the variable that stores the vocab's VID.
 */
function _prince2_vocab_varname($name) {
  return $name . '_vid';
}

/**
 * Get information about the vocabularies that are installed by this module.
 */
function _prince2_install_vocabs() {
  $t = get_t();
  $issue_types = array(
    'name' => $t('PRINCE2 Issue Types'),
  );
  return array(
    'prince2_issue_types' => $issue_types,
  );
}

/**
 * Get information about the fields that are installed by this module.
 */
function _prince2_install_node_types() {
  $t = get_t();
  $project = array(
    array(
      'field_name' => 'prince2_project_description',
      'label' => $t('Short Description'),
      'type' => 'text_long',
    ),
  );
  $issue = array(
    array(
      'field_name' => 'prince2_issue_project',
      'label' => $t('Project'),
      'type' => 'entityreference',
      'required' => TRUE,
      'settings' => array(
        'handler' => 'base',
        'handler_settings' => array(
          'target_bundles' => array('prince2_project' => 'prince2_project'),
        ),
        'target_type' => 'node',
      ),
    ),
    array(
      'field_name' => 'prince2_issue_type',
      'label' => $t('Type'),
      'type' => 'taxonomy_term_reference',
      'settings' => array(
        'allowed_values' => array(
          array('vocabulary' => 'prince2_issue_types', 'parent' => 0),
        ),
      ),
      'cardinality' => 1,
      'widget' => array('type' => 'taxonomy_autocomplete'),
    ),
    array(
      'field_name' => 'prince2_issue_description',
      'label' => $t('Description'),
      'type' => 'text_long',
    ),
  );
  return array(
    'prince2_project' => $project,
    'prince2_issue' => $issue,
  );
}

/**
 * Delete all node types, fields, and vocabularies.
 */
function prince2_uninstall() {
  drupal_load('module', 'prince2');

  // Delete all content.
  $sql = 'SELECT nid FROM {node} n WHERE n.type IN (:types)';
  $result = db_query($sql, array(':types' => array_keys(prince2_node_info())));
  $nids = array();
  foreach ($result as $row) {
    $nids[] = $row->nid;
  }
  node_delete_multiple($nids);

  // Delete all fields and instances.
  foreach (_prince2_install_node_types() as $fields) {
    foreach ($fields as $field) {
      field_delete_field($field['field_name']);
    }
  }

  // Delete all content types.
  foreach (array_keys(prince2_node_info()) as $node_type) {
    node_type_delete($node_type);
  }
  field_purge_batch(1000);

  // Delete vocabularies and associated variables.
  foreach (_prince2_install_vocabs() as $vname => $vocab) {
    $vocab_id_var = _prince2_vocab_varname($vname);
    $vid = variable_get($vocab_id_var);
    if ($vid) {
      taxonomy_vocabulary_delete($vid);
    }
    variable_del($vocab_id_var);
  }
}
